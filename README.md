# AWS SAM + Golang

Image containing Golang and AWS SAM used to build and deploy projects with Lambdas written in GO.

## Commit messages

Commit messages follow the [Gitmoji](https://gitmoji.carloscuesta.me/) pattern.