FROM golang:1.15.0-buster

RUN apt-get update && apt-get install -y python3-pip && \
    pip3 install awscli --upgrade && \
    pip3 install aws-sam-cli --upgrade

ENV SAM_CLI_TELEMETRY=0